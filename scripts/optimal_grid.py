import logging
import argparse
from typing import Sequence

import numpy
import matplotlib.pyplot as plt

from ewoksfluo.math import optimal_grid
from ewoksfluo.math import grid_utils

logger = logging.getLogger(__name__)


def plot_grid(
    scatter_coordinates: numpy.ndarray, grid_axes: Sequence[numpy.ndarray]
) -> None:
    # Generate grid intersection points for dots
    expanded_grid_coordinates = grid_utils.expanded_grid_coordinates(grid_axes)

    plt.figure(figsize=(8, 8))
    plt.scatter(
        scatter_coordinates[:, 0],
        scatter_coordinates[:, 1],
        color="blue",
        label="Data Points",
        s=50,
    )

    x_lines, y_lines = grid_axes

    yy = [y_lines.min(), y_lines.max()]
    for x in x_lines:
        plt.plot(
            [x, x],
            yy,
            color="red",
            linestyle="-",
        )
    xx = [x_lines.min(), x_lines.max()]
    for y in y_lines:
        plt.plot(
            xx,
            [y, y],
            color="red",
            linestyle="-",
        )
    plt.scatter(
        expanded_grid_coordinates[:, 0],
        expanded_grid_coordinates[:, 1],
        color="red",
        s=10,
        label="Grid Intersections",
    )

    plt.xlabel("X")
    plt.ylabel("Y")
    plt.title("Optimal Grid")
    plt.legend()
    plt.show()


def semi_regular_scatter(state: int) -> numpy.ndarray:
    initial_grid_shape = (10, 15)
    logging.debug("Initial grid shape: %s", initial_grid_shape)
    d = 0.1
    grid_axes = [numpy.arange(n) for n in initial_grid_shape]
    expanded_grid_coordinates = grid_utils.expanded_grid_coordinates(grid_axes)
    return expanded_grid_coordinates + state.uniform(
        low=-d, high=d, size=expanded_grid_coordinates.shape
    )


def full_random_scatter(state: int) -> numpy.ndarray:
    nscatter = 20
    initial_grid_shape = (10, 15)
    scale = numpy.array(initial_grid_shape)[numpy.newaxis, :]
    return state.rand(nscatter, 2) * scale


if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("matplotlib").setLevel(level=logging.WARNING)

    parser = argparse.ArgumentParser(
        description="Manual testing of optimal grid determination."
    )

    parser.add_argument(
        "--full-random",
        action="store_true",
        help="Use full random scatter if set; otherwise, use semi-regular scatter.",
    )

    parser.add_argument(
        "--fix-resolution",
        action="store_true",
        help="Do not refine the resolution.",
    )

    parser.add_argument(
        "--fix-limits",
        action="store_true",
        help="Do not refine the limits.",
    )

    args = parser.parse_args()

    state = numpy.random.RandomState(42)

    while True:
        if args.full_random:
            scatter_coordinates = full_random_scatter(state)
        else:
            scatter_coordinates = semi_regular_scatter(state)

        grid_axes = optimal_grid.optimal_grid_axes(
            scatter_coordinates,
            fix_resolution=args.fix_resolution,
            fix_limits=args.fix_limits,
        )
        plot_grid(scatter_coordinates, grid_axes)
