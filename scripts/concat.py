"""
Fast example

.. code::

    python scripts/concat.py \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95061_111615/GYYB27_roi95061_111615.h5 -s 1,1 \
        -c /data/scisoft/ewoks/ihhg37/id21/20240918/SCRIPTS/pymca/GYYB27.cfg

Slow example

.. code::

    python scripts/concat.py \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95062_111627/GYYB27_roi95062_111627.h5 -s 2,33 \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95064_111618/GYYB27_roi95064_111618.h5 -s 2,37 \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95087_111637/GYYB27_roi95087_111637.h5 -s 2,28 \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95088_111640/GYYB27_roi95088_111640.h5 -s 2,22 \
        -f /data/scisoft/ewoks/ihhg37/id21/20240918/RAW_DATA/GYYB27/GYYB27_roi95061_111615/GYYB27_roi95061_111615.h5 -s 1,1 \
        -c /data/scisoft/ewoks/ihhg37/id21/20240918/SCRIPTS/pymca/GYYB27.cfg

The proposal is /data/visitor/ihhg37/id21/20240918/RAW_DATA/GYYB27
"""

import os
import logging
import argparse
from pprint import pprint
from typing import List, Tuple

from ewoks import convert_graph
from ewoks import execute_graph
from ewoksfluo.io.blissconcat import concatenate_bliss_scans


VIRTUAL_AXES = {"sy": "<samy>+<sampy>", "sz": "<samz>+<sampz>"}
AXES_UNITS = {"samy": "mm", "samz": "mm", "sampy": "um", "sampz": "um"}
POSITIONERS = ["sy", "sz"]
DETECTOR_NAME = "fx2_det0"


def concat_and_fit(
    filenames: List[str], scan_ranges: List[Tuple[int, int]], config: str
) -> None:
    parameters = {
        "filenames": filenames,
        "scan_ranges": scan_ranges,
        "bliss_scan_uri": "concat.h5",
        "output_root_uri": "results.h5",
        "detector_name": DETECTOR_NAME,
        "config": config,
        "virtual_axes": VIRTUAL_AXES,
        "axes_units": AXES_UNITS,
        "positioners": POSITIONERS,
    }

    file_path = os.path.join(os.path.dirname(__file__), "concat.ows")

    workflow = convert_graph(file_path, dict())

    inputs = [{"name": k, "value": v, "all": True} for k, v in parameters.items()]

    result = execute_graph(workflow, inputs=inputs)
    pprint(result)


def concat(filenames: List[str], scan_ranges: List[Tuple[int, int]]) -> None:
    bliss_scan_uris = [
        f"{filename}::/{scannr}.1"
        for filename, scan_range in zip(filenames, scan_ranges)
        for scannr in range(scan_range[0], scan_range[1] + 1)
    ]
    output_root_uri = "concat.h5"

    print(
        concatenate_bliss_scans(
            bliss_scan_uris,
            output_root_uri,
            virtual_axes=VIRTUAL_AXES,
            axes_units=AXES_UNITS,
        )
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser(
        description="Manual testing of mosaic XRF imaging."
    )

    parser.add_argument(
        "-f",
        "--filenames",
        action="append",
        required=True,
        help="List of file paths to process.",
    )

    parser.add_argument(
        "-s",
        "--scan-ranges",
        action="append",
        type=lambda s: tuple(map(int, s.split(","))),
        required=True,
        help="List of scan ranges, specified as start,end (e.g., 2,33).",
    )

    parser.add_argument("-c", "--config", help="PyMca configuration file path.")

    params = parser.parse_args()

    if params.config:
        concat_and_fit(params.filenames, params.scan_ranges, params.config)
    else:
        concat(params.filenames, params.scan_ranges)
