import xml.etree.ElementTree as ET
import sys


def remove_saved_widget_geometry(file_path):
    tree = ET.parse(file_path)
    root = tree.getroot()

    for properties in root.findall(".//properties"):
        properties_dict = eval(properties.text)
        if "savedWidgetGeometry" in properties_dict:
            properties_dict["savedWidgetGeometry"] = None
        properties.text = repr(properties_dict)

    tree.write(file_path, encoding="utf-8", xml_declaration=True)
    print(f"Processed file: {file_path}")


if __name__ == "__main__":
    file_path = sys.argv[1]
    remove_saved_widget_geometry(file_path)
