FluoXAS stack with one detector
===============================

Workflow: `fluoxas_single_detector.ows <https://gitlab.esrf.fr/workflow/workflowhub/id21workflows/-/raw/main/workflows/fluoxas_single_detector.ows?inline=false>`_

.. image:: images/fluoxas_single_detector.png

The primary workflow parameters are

.. list-table::
   :header-rows: 1

   * - **Parameter**
     - **Description**
     - **Example**
   * - filenames
     - Path to the HDF5 file
     - :code:`["/data/visitor/blc15566/id21/20240722/RAW_DATA/BR_GRE/BR_GRE_roi91254_107278/BR_GRE_roi91254_107278.h5"]`
   * - scan_ranges
     - Bliss scan number range
     - :code:`[4, 84]`
   * - config
     - Path to the PyMca configuration file
     - :code:`/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/pymca/config_7_12.cfg`
   * - output_root_uri
     - Output URI root
     - :code:`/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/ewoksdemo/BR_GRE_roi91254_107278.h5::/4.1`
   * - detector_name
     - Name of the XRF detector used
     - :code:`fx2_det0`
   * - counter_name
     - Name of the counter for normalization
     - :code:`iodet`
   * - counter_normalization_template
     - Template for counter normalization
     - :code:`180000/<instrument/{}/data>`
   * - detector_normalization_template
     - Template for detector normalization
     - :code:`0.1/<instrument/{}/live_time>`
