Installation
============

You can install `ewoksfluo` in any python environment. For example create
and activate a virtual environment just for `ewoksfluo`

```bash
python3 -m venv fluoenv
source fluoenv/bin/activate
```

To install all dependencies (`Orange3` is optional)

```bash
pip install ewoksfluo pyqt5 [orange3]
```
