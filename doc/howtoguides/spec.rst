SPEC data
=========

`SPEC data <http://ftp.esrf.fr/pub/scisoft/silx/doc/SpecFileManual.pdf>` is supported by converting to
HDF5 format with the same structure as `BLISS data <https://bliss.gitlab-pages.esrf.fr/bliss/master/data/data_nexus.html>`.

For example to convert several SPEC scans to HDF5:

.. code:: python

    from ewoksfluo.io.convert import spec_to_bliss

    spec_to_bliss("/path/to/file.spec", "/path/to/file.h5", scans=[10, 11, 12])
