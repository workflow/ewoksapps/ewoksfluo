Mosaic XRF map with multiple detectors
======================================

Workflow: `mosaic_multi_detector.ows <https://gitlab.esrf.fr/workflow/workflowhub/id21workflows/-/raw/main/workflows/mosaic_multi_detector.ows?inline=false>`_

.. image:: images/mosaic_multi_detector.png

The primary workflow parameters are

.. list-table::
   :header-rows: 1

   * - **Parameter**
     - **Description**
     - **Example**
   * - filenames
     - Paths to the HDF5 files
     - :code:`['/data/visitor/blc15566/ewoks/id21/RAW_DATA/GYYB27/GYYB27_roi95062_111627/GYYB27_roi95062_111627.h5', '/data/visitor/blc15566/ewoks/id21/RAW_DATA/GYYB27/GYYB27_roi95064_111618/GYYB27_roi95064_111618.h5']`
   * - scan_ranges
     - Bliss scan numbers
     - :code:`[(2, 33), (2, 37)]`
   * - bliss_scan_uri
     - Output URI for the virtual concatenated scan
     - :code:`/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/ewoksconcat/GYYB27.h5::/1.1`
   * - virtual_axes
     - Virtual axes as a combination of real positioners
     - :code:`{'sy': '<samy>+<sampy>', 'sz': '<samz>+<sampz>'}`
   * - positioners
     - Axes of the mosaic map
     - :code:`['sy', 'sz']`
   * - configs
     - Paths to the PyMca configuration files
     - :code:`["/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/pymca/config_7_12.cfg", "/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/pymca/config_7_12.cfg"]`
   * - output_root_uri
     - Output URI root
     - :code:`/data/visitor/blc15566/id21/20240722/PROCESSED_DATA/ewoksdemo/GYYB27.h5::/4.1`
   * - detector_names
     - Names of the XRF detectors used
     - :code:`["fx2_det0", "fx2_det1"]`
   * - counter_name
     - Name of the counter for normalization
     - :code:`iodet`
   * - counter_normalization_template
     - Template for counter normalization
     - :code:`180000/<instrument/{}/data>`
   * - detector_normalization_template
     - Template for detector normalization
     - :code:`0.1/<instrument/{}/live_time>`
