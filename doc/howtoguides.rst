How-to Guides
=============

.. toctree::

    howtoguides/installation
    howtoguides/xrfmap_single_detector
    howtoguides/xrfmap_multi_detector
    howtoguides/xrfmap_multi_detector_sumspectra
    howtoguides/fluoxas_single_detector
    howtoguides/fluoxas_multi_detector
    howtoguides/mosaic_single_detector
    howtoguides/mosaic_multi_detector
    howtoguides/mosaic_multi_detector_sumspectra
    howtoguides/spec
