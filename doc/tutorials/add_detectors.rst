Multiple detectors
==================

Data from multiple detectors can be added before or after fitting.

Add after fitting
------------------

Compute the weighted sum of the peak areas, associated uncertainties and mass fractions for several detectors.

For elemental peak areas

.. math::

    A(\mathrm{Fe}) = \sum_i{\left[ W_i A_i(\mathrm{Fe}) \right] }

The variable :math:`W_i` is the weight for detector :math:`i` which is typically the inverse of the live time
multiplied by the target live time.

For their uncertainties

.. math::

    \sigma_{A}(\mathrm{Fe}) = \sqrt{ \sum_i\left[ W_i^2 \sigma_{A_i}^2(\mathrm{Fe}) \right] }

For elemental mass fractions, in addition to the detector weight we also weight for the peak area

.. math::

    M(\mathrm{Fe}) = \sum_i{\left[ W_i M_i(\mathrm{Fe}) \frac{W_i A_i(\mathrm{Fe})}{A(\mathrm{Fe})} \right] }

Add before fitting
------------------

.. warning::
    When adding XRF spectra from multiple detector, quantification can never be correct since the
    combined response of multiple detectors cannot be replaced by the response of a single theoretical detector.

The weighted sum of XRF spectra from multiple detectors is

.. math::

    \mathrm{MCA}_{\mathrm{sum}} = \sum_i\left[ W_i\mathrm{MCA}_{i} \right]
