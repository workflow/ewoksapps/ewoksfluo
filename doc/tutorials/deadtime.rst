Deadtime
========

When detecting events from a random process, a pulse is generated for each
event which has a finite time width τ. Either pulses are rejected
during this time period (non-extendable dead time) or overlapping pulses are
counted as one pulse (extendable dead time).

Deadtime fraction
-----------------

`DT`: the fraction of counts or time lost due to pulse processing

`1-DT`: the fraction of counts or time left after pulse processing

.. code::

    COUNTS_MEASURED = COUNTS_REAL * (1-DT)
    LIVE_TIME = REAL_TIME * (1-DT)

`LIVE_TIME`: the effective time of COUNTS_MEASURED

`REAL_TIME`: the effective time of COUNTS_REAL

Deadtime correction
-------------------

Counts that would have been measured if there was no pulse loss:

.. code::

        COUNTS_REAL = COUNTS_MEASURED * DT_COR
    <=> DT_COR = 1 / (1-DT) = REAL_TIME / LIVE_TIME

Counts that would have been measured if there was no pulse loss
and the exposure time was not REAL_TIME but NORM_TIME:

.. code::

        COUNTS_NORM = COUNTS_MEASURED * LT_COR
    <=> LT_COR = NORM_TIME / LIVE_TIME

The latter is referred to as a *live time correction*.
So deadtime correction is a special case of live time correction
with `NORM_TIME=REAL_TIME`. Both cannot be applied at the same time: only one or the other.
The goal is to correct measured counts to a value that would have
been measured in a specific time with a perfect detector (one
that does not loose counts and therefore does not loose time).

Counting pulses
---------------

There are three types of "counts":

    * `PHOTONS`: the real number of pulses
    * `TRIGGERS`: the number of pulses being detected
    * `TOTAL_EVENTS`: the number of pulses being accepted

Pulses are lost whenever counting something:

    * PHOTONS → TRIGGERS: pulses are lost due to overlap and
                        electronics overhead, e.g. resets in
                        a dual-channel signal processor
    * TRIGGERS → TOTAL_EVENTS: pulses are rejected if they do
                            not fulfill certain criteria

Loss of pulses: PHOTONS ➡ TRIGGERS
++++++++++++++++++++++++++++++++++

.. code::

        DT_TRIGGERS   = (PHOTONS-TRIGGERS)/PHOTONS       # lost fraction in counts
                    = (REAL_TIME-LT_TRIGGERS)/REAL_TIME  # lost fraction in time
    <=> 1-DT_TRIGGERS = TRIGGERS/PHOTONS                 # remaining fraction in counts
                    = LT_TRIGGERS/REAL_TIME              # remaining fraction in time

`LT_TRIGGERS`: time not occupied by detected pulses and electronics overhead (e.g. resets)

Loss of pulses: PHOTONS ➡ TOTAL_EVENTS
++++++++++++++++++++++++++++++++++++++

.. code::

        DT_EVENTS   = (PHOTONS-TOTAL_EVENTS)/PHOTONS    # lost fraction in counts
                    = (REAL_TIME-LT_EVENTS)/REAL_TIME   # lost fraction in time
    <=> 1-DT_EVENTS = TOTAL_EVENTS/PHOTONS              # remaining fraction in counts
                    = LT_EVENTS/REAL_TIME               # remaining fraction in time

        LT_EVENTS   = REAL_TIME.TOTAL_EVENTS/PHOTONS
                    = LT_TRIGGERS.TOTAL_EVENTS/TRIGGERS

`LT_EVENTS` (called "energy live time" by XIA): time remaining after loss due to pulse processing (pulse rejection and electronics overhead)

Correcting MCA counts
---------------------

Correct measured counts to counts of a perfect detector with exposure time `REAL_TIME`:

.. code::

    MCA_REAL = MCA * DT_COR

    DT_COR = 1/(1-DT_EVENTS) = REAL_TIME/LT_EVENTS = PHOTONS/TOTAL_EVENTS

Correct measured counts to counts of a perfect detector with exposure time `NORM_TIME`:

.. code::

    MCA_NORM = MCA * LT_COR

    LT_COR = NORM_TIME/LT_EVENTS
    LT_EVENTS = LT_TRIGGERS.TOTAL_EVENTS/TRIGGERS

XIA definitions
---------------

XIA defines the following quantities:

.. code::

    OCR       = TOTAL_EVENTS/REAL_TIME
    ICR       = TRIGGERS/LT_TRIGGERS
    LT_ENERGY = LT_EVENTS

As a result we can express `LT_EVENTS` and `DT_COR` as:

.. code::

    LT_EVENTS = LT_TRIGGERS.TOTAL_EVENTS/TRIGGERS
            = TOTAL_EVENTS/ICR
            = REAL_TIME.OCR/ICR

    DT_COR = REAL_TIME/LT_EVENTS
        = REAL_TIME.ICR/TOTAL_EVENTS
        = ICR/OCR

You can use any of these expressions depending on which quantities are provided with the MCA data. Most data processing tools can handle only deadtime correction with LT_EVENTS.

XMAP/MERCURY/SATURN
+++++++++++++++++++

.. code::

    TOTAL_EVENTS = EVENTS+OVER+UNDER

FALCONX
+++++++

.. code::

    TRIGGERS     = PULSES_ACCEPTED + PULSES_REJECTED
    TOTAL_EVENTS = PULSES_ACCEPTED
