Normalization
=============

To make XRF results comparable between each point in a single scan or across multiple
scans, they need to be normalized in two ways

- normalize the data as if it was recorded with the same *incoming X-ray flux*
- normalize the data as if it was recorded with the same *effective exposure time*

The incoming X-ray flux and effective exposure time change for each point in a scan.
In addition the later is different between detectors for the same point in a scan.

Normalization is defined by `arithmetic expression templates` which by default are

* `counter_normalization_template`: :code:`np.mean(<instrument/{}/data>)/<instrument/{}/data>`
* `detector_normalization_template`: :code:`1./<instrument/{}/live_time>`

So by default the data is normalized to 1 seconds effective exposure time and a flux that
corresponds to the average counter value. Note that this average counter value will change
between scans. To compare intensities across scans, a fixed counter value must be used.

The :code:`{}` placeholders are for

* `counter_name`: name of a counter with a single that is proportional to the incoming X-ray flux
* `detector_name(s)`: name of the XRF detector(s) for which the live time is recorded

When doing XRF quantification it is important that the arithmetic expression templates
correspond the `flux` and `time` parameters in the PyMCA configuration

.. image:: images/pymca_fundamental_parameters.png

So in this case the expression templates should be (assuming :code:`iodet=180000` counts corresponds to a flux of 3.7e10 ph/s)

* `counter_normalization_template`: :code:`180000/<instrument/{}/data>`
* `detector_normalization_template`: :code:`0.1/<instrument/{}/live_time>`

This means for example that the `Fe` counts or mass fractions will be normalized like

.. math::

    \mathrm{Fe}_{\mathrm{norm}} = \mathrm{Fe} \frac{180000}{\mathrm{iodet}} \frac{0.1}{\mathrm{live\_time}}

The associated uncertainties are also multiplied by the normalization expression.

.. math::

    \sigma_{\mathrm{Fe,norm}} = \sigma_{\mathrm{Fe}} \frac{180000}{\mathrm{iodet}} \frac{0.1}{\mathrm{live\_time}}

The active area and the distance are determined by measuring a thin-film standard when
known elemental mass fractions. Note that when you sum the spectra before fitting, the
active area and distance need to be calibrated on the sum. Theoretically you can never
replace a multiple detector geometry by one with a single active area and distance.
