Workflow results
================

Every step in an `ewoksfluo` workflow saves one of more `NXprocess <https://manual.nexusformat.org/classes/base_classes/NXprocess.html>`_
groups under the `output_uri_root`

.. image:: images/xrfmap_single_detector_results1.png

Each `NXprocess` group contains a *configuration* group with processing parameters (e.g. PyMca configuration)
and a *results* group with several `NXdata <https://manual.nexusformat.org/classes/base_classes/NXdata.html>`_ groups

.. image:: images/xrfmap_single_detector_results2.png

.. list-table::
   :header-rows: 1

   * - **NXdata group name**
     - **Description**
     - **Example**
   * - parameters
     - PyMca fit parameters
     - :code:`Ca_K`, :code:`Fe_K`, ...
   * - uncertainties
     - Standard deviation on PyMca fit parameters
     - :code:`Ca_K`, :code:`Fe_K`, ...
   * - massfractions
     - Elemental mass fractions
     - :code:`Ca_K`, :code:`Fe_K`, ...
   * - rawcounters
     - Raw BLISS counters
     - :code:`idet`, :code:`fx2_det0_events`, ...
   * - fit
     - PyMca fit + residuals
     - :code:`data`, :code:`model`, ...
   * - diagnostics
     - PyMca Least-Square fit diagnostics
     - :code:`nFreeParameters`, ...
   * - derivatives
     - PyMca Element Spectra (linear fit)
     - :code:`Ca_K`, :code:`Fe_K`, ...

The *parameters* group is plotted by default.
