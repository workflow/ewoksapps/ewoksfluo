ewoksfluo |version|
===================

*ewoksfluo* provides data processing workflows for X-ray Fluorescence.

*ewoksfluo* has been developed by the `Software group <http://www.esrf.fr/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_.

.. toctree::
    :hidden:

    tutorials
    howtoguides
    api