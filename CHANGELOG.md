# CHANGELOG.md

## Unreleased

New Features:

- Concatenate Bliss scans.

Changes:

- Avoid multi-scan confusion. It refers to a stack of identical scans, not just any number of unrelated scans.

Breaking Changes:

- Regridding is based on any motor positions, not on the expected evolution of motor positions in 2D scans.

- Drop Python 3.6 and 3.7

## 0.3.1

Bug fixes:

- Spec to Bliss: "end_time" is a dataset, not an attribute.

## 0.3.0

Changes:

- Harmonization of task parameters.
- Data viewer no longer keeps the HDF5 file open.

## 0.2.1

Bug fixes:

- Fix the `requires` section in `pyproject.toml`.

## 0.2.0

New Features:

- Support camera's in SPEC to BLISS
- Regrid with interpolation option
- Support silx 2.0

Changes:

- Remove deprecated `pkg_resources` and related methods
- Disable the data viewer until a solution has been found

## 0.1.0

Initial release. Provides tasks to

### Preprocessing

- Convert a SPEC file to BLISS format
- Pick scans from a BLISS file
- Sum detector data
- Normalize detector data

### Processing

- Fit single scan with a single detector (or a sum of detector data)
- Fit single scan with multiple detectors (in parallel)
- Fit multiple scans with multiple detectors (in parallel)

### Postprocessing

- Normalize fit results
- Regrid fit results according to motor positions
- Sum fit results
