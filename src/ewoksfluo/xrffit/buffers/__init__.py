"""Buffers receive data from pymca."""

from .pymca import PyMcaOutputBuffer  # noqa F401
from .external import ExternalOutputBuffer  # noqa F401
